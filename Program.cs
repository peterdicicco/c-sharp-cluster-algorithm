﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClusterBig
{
    class Program
    {

        private static List<Vertex> Vs = new List<Vertex>();
        private static Dictionary<string, Vertex> VertDictionary = new Dictionary<string, Vertex>();


        static void Main(string[] args)
        {
            var s1 = Stopwatch.StartNew();
            Vs = LoadData();
            s1.Stop();

            Console.WriteLine("Loading Data: {0} msec. ",s1.Elapsed.Milliseconds);


            var s2 = Stopwatch.StartNew();
            for (int i = 0; i < Vs.Count; i++)
            {

                if (i % 500 == 0)
                    Console.WriteLine("{0:F2} percent complete, Elapsed: {1} hr {2} min {3} sec ", (float)(i*100)/Vs.Count  , s2.Elapsed.Hours, s2.Elapsed.Minutes ,  s2.Elapsed.Seconds);

                List<string> contendersHD1 = HammingDistance(Vs[i].hammingCode, 1);
                List<string> contendersHD2 = HammingDistance(Vs[i].hammingCode, 2);

                foreach (var item in contendersHD1)
                {
                    if (VertDictionary.ContainsKey(item))
                        ProcessVertex(Vs[i], VertDictionary[item]);
                }

                foreach (var item in contendersHD2)
                {
                    if (VertDictionary.ContainsKey(item))
                        ProcessVertex(Vs[i], VertDictionary[item]);

                }


            }
            s1.Stop();

            Console.WriteLine("Running Loop: {0} sec. ", s2.Elapsed.Seconds);

            var dist = Vs.GroupBy(a => a.cluster);
            var ss = dist.ToList().Count;

            Console.WriteLine("Disting Clusters: {0} ", ss);



            Console.ReadLine();
        }


        private static void ProcessVertex(Vertex v1 , Vertex v2)
        {


            if (v1.cluster != v2.cluster)
            {
                Vertex.vertexCount--;

                int cl1 = v1.cluster;
                int cl2 = v2.cluster;

                int clsize1 = v1.clusterSize;
                int clsize2 = v2.clusterSize;


                //merge smaller cluster into larger one
                if (clsize1 < clsize2)
                {
                    Vs.ForEach(x =>
                    {
                        if (x.cluster == cl1)
                        {
                            x.cluster = cl2;
                            x.clusterSize = clsize1 + clsize2;
                        }

                        if (x.cluster == cl2)
                            x.clusterSize = clsize1 + clsize2;

                    });
                }
                else
                {
                    Vs.ForEach(x =>
                    {
                        if (x.cluster == cl2)
                        {
                            x.cluster = cl1;
                            x.clusterSize = clsize1 + clsize2;
                        }

                        if (x.cluster == cl1)
                            x.clusterSize = clsize1 + clsize2;

                    });

                }

            }

        }




        private static List<Vertex> LoadData()
        {
            string[] lines = File.ReadAllLines(@"p:\develop\CourseRA\Algorithms-2\Week2\clustering_big.txt");
          //  string[] lines = File.ReadAllLines(@"p:\develop\CourseRA\Algorithms-2\Week2\test3.txt");

            string[] line1 = lines[0].Split(new Char[] { ' ' });

            Vertex.bitCount = int.Parse(line1[1]);

            List<Vertex> Vs = new List<Vertex>();

            for (int i = 1; i < lines.Length; i++)
            {
                string hmc = lines[i].Replace(" ", "");

                if (VertDictionary.ContainsKey(hmc) == false)
                {
                    Vertex v = new Vertex(i, hmc);
                    VertDictionary.Add(hmc, v );
                    Vs.Add(v);
                }
            }

            Vertex.vertexCount = Vs.Count;

            return Vs;
        }


        private static void PrintVertexList()
        {
            foreach (var item in Vs)
            {
                Console.WriteLine(item);
            }
        }



        private static List<string> HammingDistance (string s, int dist)
        {
            List<string> result = new List<string>();
            if (dist==1)
            {
                for (int i = 0; i < s.Length; i++)
                {
                    StringBuilder sb = new StringBuilder(s);
                    sb[i] = (sb[i] == '0') ? '1' : '0';
                    result.Add(sb.ToString());
                }
            }

            if (dist == 2)
            {
                for (int i = 0; i < s.Length; i++)
                {
                    for (int j = i+1; j < s.Length; j++)
                    {
                        StringBuilder sb = new StringBuilder(s);
                        sb[i] = (sb[i] == '0') ? '1' : '0';
                        sb[j] = (sb[j] == '0') ? '1' : '0';
                        result.Add(sb.ToString());
                    }
                }
            }
            return result;
        }


    }


    class Vertex
    {
        public static int vertexCount;
        public static int bitCount;

        public int vertex { get; }
        public string hammingCode { get; }
        public bool explored { get; set; }
        public int cluster { get; set; }
        public int clusterSize { get; set; }


        public Vertex(int _vertex, string _hammingCode)
        {
            vertex = _vertex;
            explored = false;
            cluster = _vertex;
            clusterSize = 1;
            hammingCode = _hammingCode;
        }

        public override string ToString() => $"Vertex: {vertex}({explored}) Hamming: {hammingCode} Cluster: {cluster}:{clusterSize} ";
    }


}
